/*
class User{
  name:string
  private age:number
  constructor(name:string, age:number){
    this.name = name
    this.age = age
  }
}

//above User class can be redefine as bellow -to optimize code

class User {
  constructor(public name:string, private age:number){
  }
}
*/

/*Interface force its implementing class to use all properties defined in interface,
which helps developers to follow standard practices (have certain structure , methods or properties).
here YourAge() must have to use in User class(as its implementing AksAge interface)*/
interface Greeting {
  name: string;
}

interface AskAge {
  YourAge: () => void;
}

class User implements Greeting, AskAge {
  constructor(public name: string, private age: number) {}

  YourAge = () => {
    console.log("How old are you...?");
  };
}

//inherite properties of base class
class admin extends User {
  constructor(name: string, age: number, private permissions: string[]) {
    super(name, age);
  }
}

const userObj = new User("Akshay", 27);
console.log(userObj.name);
userObj.YourAge();
//age property wont be accessible as its a private
// console.log(userObj.age)

const input1 = document.getElementById("input1")! as HTMLInputElement;
const input2 = document.getElementById("input2")! as HTMLInputElement;
const button = document.querySelector("button");

const add = (input1: number, input2: number) => {
  return input1 + input2;
};

//literal
type PrintMode = "console" | "alert";
enum OutputMode {
  CONSOLE,
  ALERT,
}

//print value using literal condition
function printResult(res: string | number, printMode: PrintMode) {
  if (printMode === "console") {
    console.log("Addition : ", res);
  } else if (printMode === "alert") {
    alert("Addition : " + res);
  }
}

//print value using enum condition
function printResultUsingEnum(res: string | number, printMode: OutputMode) {
  if (printMode === OutputMode.CONSOLE) {
    console.log("Addition : ", res);
  } else if (printMode === OutputMode.ALERT) {
    alert("Addition : " + res);
  }
}

interface CalculationResults {
  res: number;
  print: () => void;
}

//assigned interface
type MyCalculationResults = CalculationResults[];

//create own type alise by using type keyword
// type MyCalculationResults = { res: number; print: () => void }[];

//define type of data will store in arr
// const resultArr:{res:number, print: () => void }[] = []

const resultArr: MyCalculationResults = [];

button.addEventListener("click", () => {
  const additionVal = add(+input1.value, +input2.value);
  // console.log('addition of two value', additionVal)

  const resultContainer = {
    res: additionVal,
    print() {
      console.log("result value:", this.res);
    },
  };

  resultArr.push(resultContainer);
  // printResult(resultArr);
  printResult(additionVal, "console");
  printResult(additionVal, "alert");

  printResultUsingEnum(additionVal, OutputMode.ALERT);
  printResultUsingEnum(additionVal, OutputMode.CONSOLE);

  resultArr[0].print();
});

//Generic type function
function logAndEcho<T>(value: T) {
  console.log("echoing from generic function....");
  return value;
}

logAndEcho<string>("this is value").split(" ");
