/*
class User{
  name:string
  private age:number
  constructor(name:string, age:number){
    this.name = name
    this.age = age
  }
}

//above User class can be redefine as bellow -to optimize code

class User {
  constructor(public name:string, private age:number){
  }
}
*/
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var User = /** @class */ (function () {
    function User(name, age) {
        this.name = name;
        this.age = age;
        this.YourAge = function () {
            console.log("How old are you...?");
        };
    }
    return User;
}());
//inherite properties of base class
var admin = /** @class */ (function (_super) {
    __extends(admin, _super);
    function admin(name, age, permissions) {
        var _this = _super.call(this, name, age) || this;
        _this.permissions = permissions;
        return _this;
    }
    return admin;
}(User));
var userObj = new User("Akshay", 27);
console.log(userObj.name);
userObj.YourAge();
//age property wont be accessible as its a private
// console.log(userObj.age)
var input1 = document.getElementById("input1");
var input2 = document.getElementById("input2");
var button = document.querySelector("button");
var add = function (input1, input2) {
    return input1 + input2;
};
var OutputMode;
(function (OutputMode) {
    OutputMode[OutputMode["CONSOLE"] = 0] = "CONSOLE";
    OutputMode[OutputMode["ALERT"] = 1] = "ALERT";
})(OutputMode || (OutputMode = {}));
//print value using literal condition
function printResult(res, printMode) {
    if (printMode === "console") {
        console.log("Addition : ", res);
    }
    else if (printMode === "alert") {
        alert("Addition : " + res);
    }
}
//print value using enum condition
function printResultUsingEnum(res, printMode) {
    if (printMode === OutputMode.CONSOLE) {
        console.log("Addition : ", res);
    }
    else if (printMode === OutputMode.ALERT) {
        alert("Addition : " + res);
    }
}
//create own type alise by using type keyword
// type MyCalculationResults = { res: number; print: () => void }[];
//define type of data will store in arr
// const resultArr:{res:number, print: () => void }[] = []
var resultArr = [];
button.addEventListener("click", function () {
    var additionVal = add(+input1.value, +input2.value);
    // console.log('addition of two value', additionVal)
    var resultContainer = {
        res: additionVal,
        print: function () {
            console.log("result value:", this.res);
        }
    };
    resultArr.push(resultContainer);
    // printResult(resultArr);
    printResult(additionVal, "console");
    printResult(additionVal, "alert");
    printResultUsingEnum(additionVal, OutputMode.ALERT);
    printResultUsingEnum(additionVal, OutputMode.CONSOLE);
    resultArr[0].print();
});
//Generic type function
function logAndEcho(value) {
    console.log("echoing from generic function....");
    return value;
}
logAndEcho("this is value").split(" ");
